# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FunctionInGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Name of the function - short description.', max_length=50)),
                ('comments', models.CharField(help_text=b'Any comment associated with the function.', max_length=200, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FunctionOrganiser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Name of the function - short description.', max_length=50)),
                ('comments', models.CharField(help_text=b'Any comment.', max_length=200, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nr', models.IntegerField(help_text=b'Number of the group.')),
                ('name', models.CharField(help_text=b'Name of the group.', max_length=50)),
                ('parish', models.CharField(help_text=b'Parish description linked with the group.', max_length=200)),
                ('comments', models.CharField(help_text=b'Any comment associated with the group.', max_length=400, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Organiser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_function', models.ForeignKey(help_text=b'Foreign key to function which person undertakes.', to='adminBaza.FunctionOrganiser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'First name of the person.', max_length=20)),
                ('surname', models.CharField(help_text=b'Family name of the person.', max_length=50)),
                ('pesel', models.DecimalField(help_text=b'PESEL of the person.', unique=True, max_digits=11, decimal_places=0)),
                ('birth', models.DateField(help_text=b'Date of birth of person.')),
                ('sex', models.CharField(help_text=b'Gender of the person', max_length=1, choices=[(b'M', b'Man'), (b'W', b'Woman')])),
                ('address', models.CharField(help_text=b'Address of residence oi the person.', max_length=200)),
                ('email', models.EmailField(help_text=b'E-mail address of the person.', max_length=75, blank=True)),
                ('phone', models.IntegerField(help_text=b'Phone number of the person.', null=True)),
                ('comments', models.CharField(help_text=b'Any comment relative to the person.', max_length=400, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pilgrim',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tent', models.BooleanField(help_text=b'Does pilgrim has own tent?')),
                ('comments', models.CharField(help_text=b'Any comment.', max_length=400, blank=True)),
                ('functions', models.ManyToManyField(help_text=b'Many-to-many foreign key if pilgrim has special organising functions in group.', to='adminBaza.FunctionInGroup', null=True, blank=True)),
                ('id_group', models.ForeignKey(help_text=b'Foreign key to group object.', to='adminBaza.Group')),
                ('id_person', models.ForeignKey(help_text=b'Foreign key to person object signing up to pilgrimage.', to='adminBaza.Person')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pilgrimage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Title or name of the pilgrimage', max_length=200)),
                ('dateFrom', models.DateField(help_text=b'Start date.')),
                ('dateTo', models.DateField(help_text=b'End date.')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='pilgrim',
            name='id_pilgrimage',
            field=models.ForeignKey(help_text=b'Foreign key to pilgrimage to which person is signing up.', to='adminBaza.Pilgrimage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organiser',
            name='id_person',
            field=models.ForeignKey(help_text=b'Foreign key to person which is organiser in this pilgrimage.', to='adminBaza.Person'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='organiser',
            name='id_pilgrimage',
            field=models.ForeignKey(help_text=b'Foreign key to pilgrimage in which person is organiser.', to='adminBaza.Pilgrimage'),
            preserve_default=True,
        ),
    ]
