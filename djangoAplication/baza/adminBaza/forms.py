#-*- coding: utf-8
from django import forms
import adminBaza.models as mod #import Pielgrzymi, Kontakty, Miejsca
from django.forms.extras.widgets import SelectDateWidget
from datetime import date
from django.db.models import Min
import datetime

class LoginForm(forms.Form):
    """
    Form for loginView to provide username and password.
    """
    username = forms.CharField(label="login")
    password = forms.CharField(widget=forms.PasswordInput(), label="password")

class PersonAddForm(forms.ModelForm):
    """
    Form for providing data for adding new person to the database based on the Person model.
    """
    class Meta:
        model = mod.Person
        fields = ['name', 'surname', 'pesel', 'birth', 'sex', 'address', 'email', 'phone', 'comments']
        widgets = {
            'pesel': forms.TextInput,
            'birth': SelectDateWidget(years=range(1900, date.today().year+1)),
            'phone': forms.TextInput,
        }


class PilgrimageAddForm(forms.ModelForm):
    """
    Form for providing data for adding new pilgrimage to the database based on the Pilgrimage model.
    """
    class Meta:
        model = mod.Pilgrimage
        fields = ['dateFrom', 'dateTo', 'name']
        widgets = {
            'dateFrom': SelectDateWidget,
            'dateTo': SelectDateWidget,
        }


class PilgrimAddForm(forms.ModelForm):
    """
    Form for providing data for adding new pilgrim to the database based on the Pilgrim model.
    """

    class Meta:
        model = mod.Pilgrim
        fields = ['id_person', 'id_pilgrimage', 'id_group', 'tent', 'functions', 'comments']
        widgets = {
            'functions': forms.CheckboxSelectMultiple,
        }


class OrganiserAddForm(forms.ModelForm):
    """
    Form for providing data for adding new organiser to the database based on the Organiser model.
    """
    class Meta:
        model = mod.Organiser
        fields = '__all__'

class ExtraOrganiserIdForm(forms.Form):
    """
    Form for providing data for generating extra ID document for organiser. Used in generateExtraOrganiserIdView.
    """
    name = forms.CharField()
    surname = forms.CharField()
    function = forms.CharField()
    lastLineText = forms.CharField(initial=u"WSPÓLNOTY NOWOHUCKIEJ")

class ExtraPilgrimIdForm(forms.Form):
    """
    Form for providing data for generating extra ID document for pilgrim. Used in generateExtraPilgrimIdView.
    """
    name = forms.CharField()
    surname = forms.CharField()
    group = forms.CharField()

class PilgrimFilterForm(forms.Form):
    """
    Form for filtering set of data with pilgrims displaying in pilgrimPreviewView.
    """
    # pilgrimage = forms.ModelMultipleChoiceField(queryset=mod.Pilgrimage.objects, required=False, initial=[mod.Pilgrimage.objects.latest('id')])
    pilgrimage = forms.ModelMultipleChoiceField(queryset=mod.Pilgrimage.objects, required=False)
    group = forms.ModelMultipleChoiceField(queryset=mod.Group.objects, required=False)
    function = forms.ModelMultipleChoiceField(queryset=mod.FunctionInGroup.objects, required=False)

class OrganiserFilterForm(forms.Form):
    """
    Form for filtering set of data with organisers displaying in organiserPreviewView.
    """
    # pilgrimage = forms.ModelMultipleChoiceField(queryset=mod.Pilgrimage.objects, required=False, initial=[mod.Pilgrimage.objects.latest('id')])
    pilgrimage = forms.ModelMultipleChoiceField(queryset=mod.Pilgrimage.objects, required=False) 
    function = forms.ModelMultipleChoiceField(queryset=mod.FunctionOrganiser.objects, required=False)

class PilgrimageFilterForm(forms.Form):
    """
    Form for filtering set of data with pilgrimages displaying in pilgrimagePreviewView.
    """

    minimPilgrimageDate = mod.Pilgrimage.objects.aggregate(minim=Min('dateFrom'))['minim']
    minimPilgrimageYear = date.today().year-1
    if minimPilgrimageDate:
        minimPilgrimageYear = minimPilgrimageDate.year

    dateFromBegin = forms.DateField (
        label="Begining date - from",
        initial=datetime.date.today,
        widget=SelectDateWidget(years=range(minimPilgrimageYear, date.today().year+1))
    )
    dateFromEnd = forms.DateField(
        label="Begining date - to",
        initial=datetime.date.today,
        widget=SelectDateWidget(years=range(minimPilgrimageYear, date.today().year+1))
    )

class PersonFilterForm(forms.Form):
    """
    Form for filtering set of data with peoplse displaying in personPreviewView.
    """

    name = forms.CharField(required=False)
    surname = forms.CharField(required=False)
    pesel = forms.DecimalField(widget=forms.TextInput, required=False)
