from django.contrib import admin
import adminBaza.models as mod

admin.site.register(mod.Person)
admin.site.register(mod.Pilgrimage)

admin.site.register(mod.Pilgrim)
admin.site.register(mod.Group)
# admin.site.register(mod.FunctionInGroupType)
admin.site.register(mod.FunctionInGroup)

admin.site.register(mod.FunctionOrganiser)
admin.site.register(mod.Organiser)