#-*- coding: utf-8
# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from adminBaza.models import Person, Pilgrimage, Pilgrim, Organiser
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie
from django.contrib import auth
from adminBaza.forms import LoginForm, PersonAddForm, PilgrimageAddForm, PilgrimAddForm, OrganiserAddForm, ExtraOrganiserIdForm, ExtraPilgrimIdForm, PilgrimFilterForm, OrganiserFilterForm, PilgrimageFilterForm, PersonFilterForm
from reportlab.pdfgen import canvas
from reportlab.lib import pagesizes, colors
from django.http import HttpResponse
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from django.conf import settings
import sys
import xlwt

@csrf_protect
def loginView(request):
    """
    Login view. Displays login form with login and password fields. Uses forms.loginForm form class.
    **Template:**

    :template:`adminBaza/login.html`

    """
    errors = []
    form = LoginForm()
    if request.method == 'POST':
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)
        if username and password:
            user = auth.authenticate(username=username, password=password)
            if user is not None and user.is_active:
                auth.login(request, user)
                if 'next' in request.GET:
                    return HttpResponseRedirect(request.GET['next'])
                return HttpResponseRedirect(reverse('index'))
            errors.append(u'Wrong login or password. Write again.')
        else:
            errors.append(u'Provide login and password.')
    return render(request, 'login.html', {'errors': errors, 'form': form})


@login_required    
def indexView(request):
    """
    Index view. Displays main page with links to functional parts of the aplication.
    **Template:**

    :template:`adminBaza/index.html`

    """
    return render_to_response("index.html")


def logoutView(request):
    """
    Logout view. Log outs from the aplication
    **Template:**

    :template:`adminBaza/logout.html`
    """
    auth.logout(request)
    return HttpResponseRedirect(reverse('login'))


@csrf_protect
def registerView(request):
    """
    Register view. Displays message what to do to register in system.
    **Template:**

    :template:`adminBaza/register.html`

    """
    return render_to_response("register.html")




@login_required
def personAddView(request):
    """
    Person add view. Displays form for registering new person in the system.
    **Template:**

    :template:`adminBaza/person/person_add.html`

    """
    errors = []
    form = PersonAddForm()
    if request.method == 'POST':
        # raise Exception("test")
        form = PersonAddForm(request.POST)
        if form.is_valid():
            form.save();
            return HttpResponseRedirect(reverse('personAddOK'));
        errors = form.errors;
    return render(request, 'person/person_add.html', {'errors': errors, 'form': form})


@login_required
def personAddOKView(request):
    """
    Person add OK view. Displays thanking message if adding person succeed.
    **Template:**

    :template:`adminBaza/person/person_add_ok.html`

    """
    return render_to_response("person/person_add_ok.html")


@login_required
@csrf_protect
def personPreviewView(request):
    """
    Person preview view. Displays table with all the people, or with subset filtered by filter functionality.
    **Template:**

    :template:`adminBaza/person/person_preview.html`

    """

    filterForm = PersonFilterForm()

    if request.method == "POST":
        filterForm = PersonFilterForm(request.POST)

    values = Person.objects.all()

    if 'name' in request.POST and request.POST['name']!='' and filterForm.is_valid():
        values = values.filter(name__icontains = filterForm.cleaned_data['name'])

    if 'surname' in request.POST and request.POST['surname']!='' and filterForm.is_valid():
        values = values.filter(surname__icontains = filterForm.cleaned_data['surname'])

    if 'pesel' in request.POST and request.POST['pesel']!='' and filterForm.is_valid():
        values = values.filter(pesel__icontains = filterForm.cleaned_data['pesel'])

    return render(request, 'person/person_preview.html', {'values': values, 'filterForm': filterForm})


@login_required
def personEditView(request, personId):
    """
    Person edit view. Dsplays formular similar to this one in :view:`adminBaza.personAddView` in which it's possible to edit data of person.
    :param personId: id of person to edit returned by url parser.
    **Template:**

    :template:`adminBaza/person/person_edit.html`

    """
    try:
        person = Person.objects.get(id=personId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('personPreview'))

    errors = []
    if 'edit' in request.POST:
        form = PersonAddForm(request.POST, instance=person)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('personEditOK'))
        errors = form.errors

    else:
        form = PersonAddForm(person.__dict__, instance=person)

    return render(request, 'person/person_edit.html', {'errors': errors, 'form': form, 'personId': personId})


@login_required
def personEditOKView(request):
    """
    Person edit OK view. Displays thanking message if editing person succeed.
    **Template:**

    :template:`adminBaza/person/person_edit_ok.html`

    """
    return render_to_response("person/person_edit_ok.html")


@login_required
def personDetailView(request, personId):
    """
    Person detail view. Dsplays all the data abuout the person with personId.
    :param personId: id of person to edit returned by url parser.
    **Template:**

    :template:`adminBaza/person/person_detail.html`

    """
    person = None

    try:
        person = Person.objects.get(id=personId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('personPreview'))

    return render(request, "person/person_detail.html", {'person': person})


@login_required
def pilgrimageAddView(request):
    """
    Pilgrimage add view. Displays form for adding nem pilgrimage to the system.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_add.html`

    """
    errors = []
    form = PilgrimageAddForm()
    if request.method == 'POST':
        form = PilgrimageAddForm(request.POST)
        if form.is_valid():
            form.save();
            return HttpResponseRedirect(reverse('pilgrimageAddOK'));
        errors = form.errors;
    return render(request, 'pilgrimage/pilgrimage_add.html', {'errors': errors, 'form': form})


@login_required
def pilgrimageAddOKView(request):
    """
    Pilgrimage add OK view. Displays thanking message if adding pilgrimage succeed.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_add_ok.html`

    """
    return render_to_response("pilgrimage/pilgrimage_add_ok.html")


@login_required
@csrf_protect
def pilgrimagePreviewView(request):
    """
    Pilgrimage preview view. Displays table with all pilgrimages, or with subset filtered by filter functionality.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_preview.html`

    """


    filterForm = PilgrimageFilterForm()

    if request.method == "POST":
        filterForm = PilgrimageFilterForm(request.POST)

    values = Pilgrimage.objects.all()

    if request.method == "POST" and 'clear' in request.POST:
        filterForm = PilgrimageFilterForm()
        return render(request, 'pilgrimage/pilgrimage_preview.html', {'values': values, 'filterForm': filterForm})

    if filterForm.is_valid():
        values = values.filter(dateFrom__gte=filterForm.cleaned_data['dateFromBegin']).filter(dateFrom__lte=filterForm.cleaned_data['dateFromEnd'])

    return render(request, 'pilgrimage/pilgrimage_preview.html', {'values': values, 'filterForm': filterForm})

@login_required
def pilgrimageEditView(request, pilgrimageId):
    """
    Pilgrimage edit view. Dsplays form similar to this one in :view:`adminBaza.pilgrimageAddView` in which it's possible to edit data of pilgrimage.
    :param pilgrimageId: id of pilgrimage to edit returned by url parser.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_edit.html`

    """

    pilgrimage = None

    try:
        pilgrimage = Pilgrimage.objects.get(id=pilgrimageId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('pilgrimagePreview'))

    errors = []
    if 'edit' in request.POST:
        form = PilgrimageAddForm(request.POST, instance=pilgrimage)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('pilgrimageEditOK'))
        errors = form.errors
    else:
        form = PilgrimageAddForm(pilgrimage.__dict__, instance=pilgrimage)
    return render(request, 'pilgrimage/pilgrimage_edit.html', {'errors': errors, 'form': form, 'pilgrimageId': pilgrimageId})


@login_required
def pilgrimageEditOKView(request):
    """
    Pilgrimage edit OK view. Displays thanking message if editing pilgrimage succeed.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_edit_ok.html`

    """
    return render_to_response("pilgrimage/pilgrimage_edit_ok.html")

@login_required
def pilgrimageDetailView(request, pilgrimageId):
    """
    Pilgrimage detail view. Dsplays all the data about the pilgrimage with pilgrimageId.
    :param pilgrimageId: id of pilgrimage to edit returned by url parser.
    **Template:**

    :template:`adminBaza/pilgrimage/pilgrimage_detail.html`

    """

    pilgrimage = None

    try:
        pilgrimage = Pilgrimage.objects.get(id=pilgrimageId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('pilgrimagePreview'))

    return render(request, "pilgrimage/pilgrimage_detail.html", {'pilgrimage': pilgrimage})



@login_required
def pilgrimAddView(request):
    """
    Pilgrim add view. Displays form for adding nem pilgrima to the system. See: :model:`adminBaza.Pilgrim`
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_add.html`

    """
    errors = []
    form = PilgrimAddForm()
    if request.method == 'POST':
        form = PilgrimAddForm(request.POST)
        if form.is_valid():
            form.save();
            return HttpResponseRedirect(reverse('pilgrimAddOK'));
        errors = form.errors;
    return render(request, 'pilgrim/pilgrim_add.html', {'errors': errors, 'form': form})


@login_required
def pilgrimAddOKView(request):
    """
    Pilgrim add OK view. Displays thanking message if adding pilgrim succeed.
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_add_ok.html`

    """
    return render_to_response("pilgrim/pilgrim_add_ok.html")


@login_required
@csrf_protect
def pilgrimPreviewView(request):
    """
    Pilgrim preview view. Displays table with all pilgrims, or with subset filtered by filter functionality. Gives also opportunity to export displaying subset to xls format using :view:`adminBaza/exportToXLSView`.
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_preview.html`

    """

    filterForm = PilgrimFilterForm()
    if request.method == "POST" and 'filter' in request.POST:
        filterForm = PilgrimFilterForm(request.POST)

    values = Pilgrim.objects.all()

    #print >>sys.stderr, 'Filter: ' + unicode([gr.id for gr in filterForm.cleaned_data['group']])
    if 'pilgrimage' in request.POST and filterForm.is_valid():
        values = values.filter(id_pilgrimage__in = [pilgr.id for pilgr in filterForm.cleaned_data['pilgrimage']])

    # elif filterForm.is_valid():# and filterForm.cleaned_data['pilgrimage']:
    #     values = values.filter(id_pilgrimage = Pilgrimage.objects.latest('id').id)
    #     print >>sys.stderr, filterForm.cleaned_data['pilgrimage']

    if 'group' in request.POST and filterForm.is_valid():
        values = values.filter(id_group__in = [gr.id for gr in filterForm.cleaned_data['group']])

    if 'function' in request.POST and filterForm.is_valid():
        values = values.filter(functions__in = [funct.id for funct in filterForm.cleaned_data['function']])

    if request.method == "POST" and 'generateXLS' in request.POST:
        columnLabels = [(u'First Name', 3000),
                        (u'Surname', 3000),
                        (u'PESEL', 3000),
                        (u'Birth', 3000),
                        (u'Sex', 2000),
                        (u'Address', 7000),
                        (u'e-mail', 5000),
                        (u'Phone', 4000),
                        (u'Person comments', 5000),
                        (u'Pilgrimage name', 5000),
                        (u'Date From', 3000),
                        (u'Date To', 3000),
                        (u'Group nr', 2000),
                        (u'Group name', 4000),
                        (u'Parish', 5000),
                        (u'Tent', 2000),
                        (u'Functions', 5000),
                        (u'Comments', 5000)]

        objects = []

        for pilgrim in values:
            obj = []

            obj.append(unicode(pilgrim.id_person.name))
            obj.append(unicode(pilgrim.id_person.surname))
            obj.append(unicode(pilgrim.id_person.pesel))
            obj.append(unicode(pilgrim.id_person.birth))
            obj.append(unicode(pilgrim.id_person.get_sex_display()))
            obj.append(unicode(pilgrim.id_person.address))
            obj.append(unicode(pilgrim.id_person.email))
            obj.append(unicode(pilgrim.id_person.phone))
            obj.append(unicode(pilgrim.id_person.comments))
            obj.append(unicode(pilgrim.id_pilgrimage.name))
            obj.append(unicode(pilgrim.id_pilgrimage.dateFrom))
            obj.append(unicode(pilgrim.id_pilgrimage.dateTo))
            obj.append(unicode(pilgrim.id_group.nr))
            obj.append(unicode(pilgrim.id_group.name))
            obj.append(unicode(pilgrim.id_group.parish))
            obj.append(unicode(pilgrim.tent))
            obj.append([fun[1] + u" " for fun in pilgrim.functions.values_list()])
            obj.append(unicode(pilgrim.comments))

            objects.append(obj)

        return exportToXLSView(request, columnLabels, objects, 'pilgirims', 'pilgrims')


    return render(request, 'pilgrim/pilgrim_preview.html', {'values': values, 'filterForm': filterForm})


@login_required
def pilgrimEditView(request, pilgrimId):
    """
    Pilgrim edit view. Displays form similar to this one in :view:`adminBaza.pilgrimAddView` in which it's possible to edit data of the pilgrim.
    :param pilgrimId: id of pilgrim to edit returned by url parser.
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_edit.html`

    """

    pilgrim = None

    try:
        pilgrim = Pilgrim.objects.get(id=pilgrimId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('pilgrimPreview'))

    errors = []
    if 'edit' in request.POST:
        form = PilgrimAddForm(request.POST, instance=pilgrim)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('pilgrimEditOK'))
        errors = form.errors
    else:
        form = PilgrimAddForm(instance=pilgrim)

    return render(request, 'pilgrim/pilgrim_edit.html', {'errors': errors, 'form': form, 'pilgrimId': pilgrimId})


@login_required
def pilgrimEditOKView(request):
    """
    Pilgrim edit OK view. Displays thanking message if editing pilgrim succeed.
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_edit_ok.html`

    """
    return render_to_response("pilgrim/pilgrim_edit_ok.html")


@login_required
@csrf_protect
@ensure_csrf_cookie
def pilgrimDetailView(request, pilgrimId):
    """
    Pilgrim detail view. Displays all the data about the pilgrim with pilgrimId. Gives also possibility to generate ID in pdf format in A7 size and matching text positions to standard template using :view:`adminBaza.generatePilgrimIdView`.
    :param pilgrimId: id of pilgrim to edit returned by url parser.
    **Template:**

    :template:`adminBaza/pilgrim/pilgrim_detail.html`

    """

    pilgrim = None

    try:
        pilgrim = Pilgrim.objects.get(id=pilgrimId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('pilgrimPreview'))

    return render(request, "pilgrim/pilgrim_detail.html", {'pilgrim': pilgrim})


@login_required
@csrf_protect
def generatePilgrimIdView(request, pilgrimId):
    """
    Generates pilgrim ID document view for pilgrim with pilgrimId in pdf format in A7 size and matching text positions to standard template.
    :param pilgrimId: Id of pilgrim for which should be generated ID document.
    """

    pilgrim = None

    try:
        pilgrim = Pilgrim.objects.get(id=pilgrimId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('pilgrimPreview'))

    name = unicode.upper(pilgrim.id_person.name)
    surname = unicode.upper(pilgrim.id_person.surname)
    group = unicode(pilgrim.id_group.nr)

    if not pilgrim.functions.exists():
        return templateReturnPilgrimIdView(request, name, surname, group)

    functionLines = []
    function = unicode("")
    for fun in pilgrim.functions.all():
        if (len(function)==0):
            function += unicode.upper(fun.name)
        else:
            function += unicode.upper(u"/")
            function += unicode.upper(fun.name)

    if len(function)>15:
        functionLines.append(function)
        function = unicode("")

    function += u" GRUPY " + group
    functionLines.append(function)

    return templateReturnOrganiserIdView(request, name, surname, functionLines, u"WSPÓLNOTY NOWOHUCKIEJ")


@login_required
def organiserAddView(request):
    """
    Organiser add view. Displays form for adding nem organiser to the system.
    **Template:**

    :template:`adminBaza/organiser/organiser_add.html`

    """
    errors = []
    form = OrganiserAddForm()
    if request.method == 'POST':
        form = OrganiserAddForm(request.POST)
        if form.is_valid():
            form.save();
            return HttpResponseRedirect(reverse('organiserAddOK'));
        errors = form.errors;
    return render(request, 'organiser/organiser_add.html', {'errors': errors, 'form': form})


@login_required
def organiserAddOKView(request):
    """
    Organiser add OK view. Displays thanking message if adding organiser succeed.
    **Template:**

    :template:`adminBaza/organiser/organiser_add_ok.html`

    """
    return render_to_response("organiser/organiser_add_ok.html")


@login_required
@csrf_protect
def organiserPreviewView(request):
    """
    Organiser preview view. Displays table with all organisers, or with subset filtered by filter functionality. Gives also opportunity to export displaying subset to xls format using :view:`adminBaza/exportToXLSView`.
    **Template:**

    :template:`adminBaza/organiser/organiser_preview.html`

    """

    filterForm = OrganiserFilterForm()
    if request.method == "POST":
        filterForm = OrganiserFilterForm(request.POST)

    values = Organiser.objects.all()

    if 'pilgrimage' in request.POST and filterForm.is_valid():
        values = values.filter(id_pilgrimage__in = [pilgr.id for pilgr in filterForm.cleaned_data['pilgrimage']])

    if 'function' in request.POST and filterForm.is_valid():
        values = values.filter(id_function__in = [funct.id for funct in filterForm.cleaned_data['function']])

    if request.method == "POST" and 'generateXLS' in request.POST:
        columnLabels = [(u'First Name', 3000),
                        (u'Surname', 3000),
                        (u'PESEL', 3000),
                        (u'Birth', 3000),
                        (u'Sex', 2000),
                        (u'Address', 7000),
                        (u'e-mail', 5000),
                        (u'Phone', 4000),
                        (u'Person comments', 5000),
                        (u'Pilgrimage name', 5000),
                        (u'Date From', 3000),
                        (u'Date To', 3000),
                        (u'Function', 5000)]

        objects = []

        for organiser in values:
            obj = []

            obj.append(unicode(organiser.id_person.name))
            obj.append(unicode(organiser.id_person.surname))
            obj.append(unicode(organiser.id_person.pesel))
            obj.append(unicode(organiser.id_person.birth))
            obj.append(unicode(organiser.id_person.get_sex_display()))
            obj.append(unicode(organiser.id_person.address))
            obj.append(unicode(organiser.id_person.email))
            obj.append(unicode(organiser.id_person.phone))
            obj.append(unicode(organiser.id_person.comments))
            obj.append(unicode(organiser.id_pilgrimage.name))
            obj.append(unicode(organiser.id_pilgrimage.dateFrom))
            obj.append(unicode(organiser.id_pilgrimage.dateTo))
            obj.append(unicode(organiser.id_function))

            objects.append(obj)

        return exportToXLSView(request, columnLabels, objects, 'pilgirims', 'pilgrims')

    return render(request, 'organiser/organiser_preview.html', {'values': values, 'filterForm': filterForm})


@login_required
def organiserEditView(request, organiserId):
    """
    Organiser edit view. Displays form similar to this one in :view:`adminBaza.organiserAddView` in which it's possible to edit data of the organiser.
    :param organiserId: id of organiser to edit returned by url parser.
    **Template:**

    :template:`adminBaza/organiser/organiser_edit.html`

    """
    try:
        organiser = Organiser.objects.get(id=organiserId) # może nie być tego obiektu w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('organiserPreview'))

    errors = []
    if 'edit' in request.POST:
        form = OrganiserAddForm(request.POST, instance=organiser)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('organiserEditOK'))
        errors = form.errors
    else:
        form = OrganiserAddForm(instance=organiser)

    return render(request, 'organiser/organiser_edit.html', {'errors': errors, 'form': form, 'organiserId': organiserId})


@login_required
def organiserEditOKView(request):
    """
    Organiser edit OK view. Displays thanking message if editing organiser succeed.
    **Template:**

    :template:`adminBaza/organiser/organiser_edit_ok.html`

    """
    return render_to_response("organiser/organiser_edit_ok.html")


@login_required
@csrf_protect
@ensure_csrf_cookie
def organiserDetailView(request, organiserId):
    """
    Organiser detail view. Displays all the data about the organiser with organiserId. Gives also possibility to generate ID in pdf format in A6 size and matching text positions to standard template using :view:`adminBaza.generateOrganiserIdView`.
    :param organiserId: id of organiser to edit returned by url parser.
    **Template:**

    :template:`adminBaza/organiser/organiser_detail.html`

    """

    organiser = None

    try:
        organiser = Organiser.objects.get(id=organiserId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('organiserPreview'))

    return render(request, "organiser/organiser_detail.html", {'organiser': organiser})


@login_required
@csrf_protect
def generateOrganiserIdView(request, organiserId):
    """
    Generates organiser ID document view for organiser with organiserId in pdf format in A6 size and matching text positions to standard template.
    :param organiserId: Id of organiser for which should be generated ID document.
    """

    organiser = None

    try:
        organiser = Organiser.objects.get(id=organiserId) # może nie być tego ziomka w bazie
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('organiserPreview'))

    name = unicode.upper(organiser.id_person.name)
    surname = unicode.upper(organiser.id_person.surname)
    functionLines = [unicode.upper(organiser.id_function.name)]

    return templateReturnOrganiserIdView(request, name, surname, functionLines, u"WSPÓLNOTY NOWOHUCKIEJ")

def generateExtraOrganiserIdView(request):
    """
    Extra Organiser ID view. Displays form for generating ID the same as for organiser but for data taken from form, not from DB.
    **Template:**

    :template:`adminBaza/extras/extraOrganiserId.html`

    """

    errors = []
    if 'name' in request.POST and 'surname' in request.POST and 'function' in request.POST and 'lastLineText' in request.POST:
        form = ExtraOrganiserIdForm(request.POST)
        if form.is_valid():
            name = unicode.upper(request.POST['name'])
            surname = unicode.upper(request.POST['surname'])
            function = unicode.upper(request.POST['function'])
            lastLineText = unicode.upper(request.POST['lastLineText'])
            return templateReturnOrganiserIdView(request, name, surname, [function], lastLineText)
        errors = form.errors
    else:
        form = ExtraOrganiserIdForm()

    return render(request, 'extras/extraOrganiserId.html', {'errors': errors, 'form': form})


def generateExtraPilgrimIdView(request):
    """
    Extra Pilgrim ID view. Displays form for generating ID the same as for pilgrim but for data taken from form, not from DB.
    **Template:**

    :template:`adminBaza/extras/extraPilgrimId.html`

    """

    errors = []
    if 'name' in request.POST and 'surname' in request.POST and 'group' in request.POST:
        form = ExtraPilgrimIdForm(request.POST)
        if form.is_valid():
            name = unicode.upper(request.POST['name'])
            surname = unicode.upper(request.POST['surname'])
            group = unicode.upper(request.POST['group'])
            return templateReturnPilgrimIdView(request, name, surname, group)
        errors = form.errors
    else:
        form = ExtraPilgrimIdForm()

    return render(request, 'extras/extraPilgrimId.html', {'errors': errors, 'form': form})


def templateReturnOrganiserIdView(request, name, surname, functionLines, lastLineText):
    """
    Function which returns Organiser ID ducument for suitable data from argumants in pdf format and A6 format witch text positioned on proper places matching to the template.
    :param name: Name which should be printed on the document.
    :param surname: Surname which should be printed on the document.
    :param functionLines: Table of lines with function description
    :param lastLineText: Extra line at the end.
    :return: Return response in pdf format which can be returned by view.
    """
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="organiserId.pdf"'

    p = canvas.Canvas(response)
    p.setPageSize(pagesizes.A6)

    pdfmetrics.registerFont(TTFont('Verdana', settings.SITE_ROOT + settings.STATIC_URL+'Verdana.ttf'))

    actY = 15

    p.setFont('Verdana', 18)
    p.setFillColor(colors.darkblue)

    p.drawCentredString(pagesizes.A6[0]/2, actY, lastLineText)
    actY += 22

    for line in reversed(functionLines):
        p.drawCentredString(pagesizes.A6[0]/2, actY, line)
        actY += 22

    actY += 10
    p.setFillColor(colors.black)
    p.drawCentredString(pagesizes.A6[0]/2, actY, name + " " + surname)

    p.showPage()
    p.save()
    return response

def templateReturnPilgrimIdView(request, name, surname, groupNr):
    """
    Function which returns Pilgrim ID ducument for suitable data from argumants in pdf format and A7 format witch text positioned on proper places matching to the template.
    :param name: Name which should be printed on the document.
    :param surname: Surname which should be printed on the document.
    :param groupNr: Number of group to which pilgrim belong, and which should be printed on the ID document.
    :return: Return response in pdf format which can be returned by view.
    """
    """
    :param functionLines: Table of lines with function description
    :param lastLineText: Extra line at the end.
    :return: Return response in pdf format which can be returned by view.
    """
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="organiserId.pdf"'

    A7 = (pagesizes.A6[0]/2, pagesizes.A6[1]/2)

    p = canvas.Canvas(response)
    p.setPageSize(A7)

    pdfmetrics.registerFont(TTFont('Verdana', settings.SITE_ROOT + settings.STATIC_URL+'Verdana.ttf'))

    p.setFillColor(colors.black)
    p.setFont('Verdana', 10)

    p.drawCentredString(A7[0]/2, 15, name + " " + surname)
    p.drawCentredString(120, 50, unicode(groupNr))

    p.showPage()
    p.save()
    return response


def exportToXLSView(request, columnLabels, rows, sheetName, fileName):
    """
    Function which returns xls document which can be returned by any view with cells filled with data from arguments.
    :param columnLabels: List of pairs: (label to display, width of the colum).
    :param rows: Array of rows which will be displayed in next lines.
    :param sheetName: Name of sheet in document.
    :param fileName: Name of generated file.
    :return: Return response in xls format which can be returned by view.
    """
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=' + fileName + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet(sheetName)

    row_num = 0

    '''
    columns = [
        (u"ID", 2000),
        (u"Title", 6000),
        (u"Description", 8000),
    ]
    '''

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    for col_num in xrange(len(columnLabels)):
        ws.write(row_num, col_num, columnLabels[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columnLabels[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    for obj in rows:
        row_num += 1
        row = [elem for elem in obj]
        for col_num in xrange(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response

@login_required 
def helpView(request):
    """
    Help view. Displays user documentation - main functionalities of the system.
    **Template:**

    :template:`adminBaza/help.html`

    """
    return render_to_response("help.html")
