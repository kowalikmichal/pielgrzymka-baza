# -*- coding: utf-8
from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.

class Person(models.Model):
    """
    This class provides model for every person which can occur in database. It's needed to enter person object before ie. registering person to pilgrimage as pilgrim or organiser.
    """
    SEX_CHOICES = (('M', 'Man'), ('W', 'Woman'))

    name = models.CharField(max_length=20, help_text="First name of the person.")
    surname = models.CharField(max_length=50, help_text="Family name of the person.")
    pesel = models.DecimalField(max_digits=11, decimal_places=0, unique=True, help_text="PESEL of the person.")
    birth = models.DateField(help_text="Date of birth of person.")
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, help_text="Gender of the person")
    address = models.CharField(max_length=200, help_text="Address of residence oi the person.")
    email = models.EmailField(blank=True, help_text="E-mail address of the person.")
    phone = models.IntegerField(null=True, help_text="Phone number of the person.")
    comments = models.CharField(max_length=400, blank=True, help_text="Any comment relative to the person.")

    def clean_fields(self, exclude=None):
        """
        This method provides extra check for constraints for the Person model. Merged with field property checks if PESEL value has exactly 11 digits.
        :param exclude: None - No fields are excluded.
        :raises Raises ValidationError if validating of PESEL value fails.
        """
        if self.pesel<=(10**10):
            raise ValidationError('Insert proper PESEL value.')

    def __unicode__(self):
        """
        Returns unicode object for model returning catenation of name, durname and PESEL value.
        :return: unicode objcet for model.
        """
        return self.name + " " + self.surname + " " + unicode(self.pesel);


class Pilgrimage(models.Model):
    """
    This class represents model for the pilgrimage.
    """
    name = models.CharField(max_length=200, help_text="Title or name of the pilgrimage")
    dateFrom = models.DateField(help_text="Start date.")
    dateTo = models.DateField(help_text="End date.")

    def clean(self):
        """
        Check the constraints for objects. Checks if begin date is before end date
        :raises Raises ValidationError if dates are not correct.
        """
        if self.dateFrom >= self.dateTo:
            raise ValidationError('Start date is after end date')

    def __unicode__(self):
        """
        Return unicode object for model returning name and year of begin date of pilgrimage.
        :return: unicode objcet for model.
        """
        return self.name + " " + unicode(self.dateFrom.year)


class Group(models.Model):
    """
    This class represents model for the group in the pilgrimages, smaller subgroup of all the people linked with exact parish.
    """
    nr = models.IntegerField(help_text="Number of the group.");
    name = models.CharField(max_length=50, help_text="Name of the group.")
    parish = models.CharField(max_length=200, help_text="Parish description linked with the group.")
    comments = models.CharField(max_length=400, blank=True, help_text="Any comment associated with the group.")

    def __unicode__(self):
        """
        Returns unicode object for model returning catenation of number of group and name.
        :return: unicode object for model.
        """
        return unicode(self.nr) + " - " + unicode(self.name)


class FunctionInGroup(models.Model):
    """
    This class represents model for the organising functions performed in the proper groups.
    """
    name = models.CharField(max_length=50, help_text="Name of the function - short description.")
    comments = models.CharField(max_length=200, blank=True, help_text="Any comment associated with the function.")

    def __unicode__(self):
        """
        Returns unicode object for model returning its name.
        :return: unicode object for model.
        """
        return self.name


class Pilgrim(models.Model):
    """
    This class represents model for pilgrim participating in specific pilgrimage in specific group and properties. This class can be treated as model for signing up action to concrete pilgrimage by person defined in :model:`adminBaza.Person` model.
    """
    id_person = models.ForeignKey(Person, help_text="Foreign key to person object signing up to pilgrimage.")
    id_pilgrimage = models.ForeignKey(Pilgrimage, help_text="Foreign key to pilgrimage to which person is signing up.")
    id_group = models.ForeignKey(Group, help_text="Foreign key to group object.")
    tent = models.BooleanField(help_text="Does pilgrim has own tent?")
    functions = models.ManyToManyField(FunctionInGroup, null=True, blank=True, help_text="Many-to-many foreign key if pilgrim has special organising functions in group.")
    comments = models.CharField(max_length=400, blank=True, help_text="Any comment.")


class FunctionOrganiser(models.Model):
    """
    This class represents model for this organising function performed by main organisers - the base."
    """
    name = models.CharField(max_length=50, help_text="Name of the function - short description.")
    comments = models.CharField(max_length=200, blank=True, help_text="Any comment.")

    def __unicode__(self):
        """
        Returns unicode object for model returning its name.
        :return: unicode object for model.
        """
        return self.name


class Organiser(models.Model):
    """
    This class represents model for organiser participating in specific pilgrimage. This class can be treated as model for signing up action to concrete pilgrimage by person defined in :model:`adminBaza.Person` model.
    """
    id_person = models.ForeignKey(Person, help_text="Foreign key to person which is organiser in this pilgrimage.")
    id_pilgrimage = models.ForeignKey(Pilgrimage, help_text="Foreign key to pilgrimage in which person is organiser.")
    id_function = models.ForeignKey(FunctionOrganiser, help_text="Foreign key to function which person undertakes.")