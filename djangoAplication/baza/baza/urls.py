from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from adminBaza import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url("^$", views.indexView, name='index'),
    url(r"^login/$", views.loginView, name='login'),
    url(r"^register/$", views.registerView, name='register'),
    url(r"^index/$", views.indexView, name='index'),
    url(r"^logout/$", views.logoutView, name='logout'),

    url(r"^person/add/$", views.personAddView, name='personAdd'),
    url(r"^person/add/ok/$", views.personAddOKView, name='personAddOK'),
    url(r"^person/preview/$", views.personPreviewView, name='personPreview'),
    url(r"^person/edit/(?P<personId>\d+)$", views.personEditView, name='personEdit'),
    url(r"^person/edit/ok$", views.personEditOKView, name='personEditOK'),
    url(r"^person/detail/(?P<personId>\d+)$", views.personDetailView, name='personDetail'),

    url(r"^pilgrimage/add/$", views.pilgrimageAddView, name='pilgrimageAdd'),
    url(r"^pilgrimage/add/ok/$", views.pilgrimageAddOKView, name='pilgrimageAddOK'),
    url(r"^pilgrimage/preview/", views.pilgrimagePreviewView, name='pilgrimagePreview'),
    url(r"^pilgrimage/edit/(?P<pilgrimageId>\d+)$", views.pilgrimageEditView, name='pilgrimageEdit'),
    url(r"^pilgrimage/edit/ok$", views.pilgrimageEditOKView, name='pilgrimageEditOK'),
    url(r"^pilgrimage/detail/(?P<pilgrimageId>\d+)$", views.pilgrimageDetailView, name='pilgrimageDetail'),

    url(r"^pilgrim/add/$", views.pilgrimAddView, name='pilgrimAdd'),
    url(r"^pilgrim/add/ok/$", views.pilgrimAddOKView, name='pilgrimAddOK'),
    url(r"^pilgrim/preview/", views.pilgrimPreviewView, name='pilgrimPreview'),
    url(r"^pilgrim/edit/(?P<pilgrimId>\d+)$", views.pilgrimEditView, name='pilgrimEdit'),
    url(r"^pilgrim/edit/ok$", views.pilgrimEditOKView, name='pilgrimEditOK'),
    url(r"^pilgrim/detail/(?P<pilgrimId>\d+)$", views.pilgrimDetailView, name='pilgrimDetail'),

    url(r"^pilgrim/generateId/(?P<pilgrimId>\d+)$", views.generatePilgrimIdView, name='generatePilgrimId'),

    url(r"^organiser/add/$", views.organiserAddView, name='organiserAdd'),
    url(r"^organiser/add/ok/$", views.organiserAddOKView, name='organiserAddOK'),
    url(r"^organiser/preview/", views.organiserPreviewView, name='organiserPreview'),
    url(r"^organiser/edit/(?P<organiserId>\d+)$", views.organiserEditView, name='organiserEdit'),
    url(r"^organiser/edit/ok$", views.organiserEditOKView, name='organiserEditOK'),
    url(r"^organiser/detail/(?P<organiserId>\d+)$", views.organiserDetailView, name='organiserDetail'),

    url(r"^organiser/generateId/(?P<organiserId>\d+)$", views.generateOrganiserIdView, name='generateOrganiserId'),

    url(r"^organiser/extraId$", views.generateExtraOrganiserIdView, name='extraOrganiserId'),
    url(r"^pilgrim/extraId$", views.generateExtraPilgrimIdView, name='extraPilgrimId'),

    url(r"^help/$", views.helpView, name='help'),


    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
