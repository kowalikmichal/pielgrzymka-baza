To run aplication it's necessery to have:
mysql server (ie. sudo apt-get install mysql-server)
python 2.7 (ie. sudo apt-get install python2.7)
Django 1.6 (ie. sudo apt-get install python-django)
Django MySQLdb module (ie. sudo apt-get install python-mysqldb)
Python ReportLab (ie. sudo pip install reportlab)
Python xlwt (ie. deb http://us.archive.ubuntu.com/ubuntu saucy main universe, sudo apt-get update, sudo apt-get install python-xlwt)


To run aplication firstly it's needed to run 'sql.sql' script on mysql server, which will create user for aplication and then create database 'pielgrzymka'. Then sript will assign proper priviliges for created user.
Then run mysql-convert-utf8.py script for changing default charset of database (allow polish signs).
To make sql database models from python models it's needed to run: python manage.py makamigrations adminBaza
Then to apply it to database it's needed to run: python manage.py migrate
Then to make superuser in system it's needed to run: python manage.py createsuperuser then it's necessery to provide login, e-mail and password (root, test@test.pl, ...)

To run development server use:
python manage.py runserver
